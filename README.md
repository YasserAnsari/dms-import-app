# README #

## What is this repository for? ##

An app to create shadow metadata files which can be used by the Alfresco bulk import tool to upload documents. 

## How do I get set up? ##

### Method 1 - Install from local archive file: ###

Copy over tar.gz file and run:

    pip install <filename>.tar.gz
 
This should install the code.

You may need to install some of the packages below to make it work.

### Method 2 - Zip file ###

Copy over python code zip file and unzip.

### Configuration ###

To connect to the iSeries, some configuration needs to be added to dmsrepowalker.py:

    # iSeries config.
    AS400_DRIVER = 'com.ibm.as400.access.AS400JDBCDriver'
    ISERIES_CONFIG = (
            'jdbc:as400://tracey.servers.jhc.co.uk;'
            'ccsid=285;'
            'translate binary=true;'
            'naming=system;'
            'prompt=false;'
            'libraries=F63QULDJBS,QTEMP,F63QUALITY,F63FIGARO,QGPL,JHCJUTIL,SUTIL'
            )
    ISERIES_USER = 'ANSARIY'
    ISERIES_PASSWORD = 'PASSWORD'
    
This section in the code needs to be changed to have a relevant user/password and the ISERIES_CONFIG with the server location, ccsid and libraries. 

## How do I run it? ##

From the dmsrepowalker directory

    python dmsrepowalker.py -r <source> -m build

To teardown metadata

    python dmsrepowalker.py -r <source> -m teardown

## Additional commands and packages which may or may not be required to run ##

You have to enable the EPEL repo, use:

    yum --enablerepo=extras install epel-release

This command will install the correct EPEL repository for the CentOS version you are running.
After this you will be able to install python-pip.

    yum -y install python-pip

    sudo yum install gcc

    sudo yum groupinstall 'Development Tools'

If you get this error,

    " error: command 'gcc' failed with exit status 1 "
    
the installation failed because of missing python-devel and some dependencies.

The best way to correct gcc problem is to reinstall gcc , gcc-c++ and dependencies.

For python 2.7

    $ sudo yum -y install gcc gcc-c++ kernel-devel
    $ sudo yum -y install python-devel libxslt-devel libffi-devel openssl-devel

Additional installs may be required:

    sudo pip install JayDeBeApi

    sudo pip install jinja2

    yum install java-1.8.0-openjdk-devel.x86_64

    pip install tqdm
