"""A setuptools based setup module."""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='dms-repo-walker',
    
    version='1.2.0.dev1',  # Development release
    
     # The project's main homepage.
    url='https://bitbucket.org/YasserAnsari/dms-import-app',
    
    author='Yasser Ansari',
    author_email='yasser.ansari@jhc.co.uk',
    
    license='MIT',
    
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',
        
                
        'Intended Audience :: End Users/Desktop',
        
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 2',
        
    ],
            
    # What does your project relate to?
    keywords='dms repo walker metadata creator',
    
    # You can just specify the packages manually here if your project is
    # simple. Or you can use find_packages().
    packages=find_packages(),
    
    # List run-time dependencies here.  These will be installed by pip when
    # your project is installed. For an analysis of "install_requires" vs pip's
    # requirements files see:
    # https://packaging.python.org/en/latest/requirements.html    
    install_requires=['JayDeBeApi', 'jinja2'],

    # If there are data files included in your packages that need to be
    # installed, specify them here.  If using Python 2.6 or less, then these
    # have to be included in MANIFEST.in as well.
    package_data={
        'templates': ['template.mock.xml'],
        'jt400': ['jt400.jar']
    },    
    
)