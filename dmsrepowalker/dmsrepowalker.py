 # -*- coding: utf-8 -*-
"""DMS Repo Walker tool.

Created on Wed Nov  8 11:02:02 2017

This tool can be used to examine a file system folder structure and the
documents contained within it. It's primary purpose is to create a sidecar
XML file for each document that contains Figaro metadata information about
that document.

The metadata associated with a document is inferred from the file naming
convention, as provided by a client.

Once metadata sidecar files have been generated the Alfresco bulk upload
tool can be used to ingest all documents into Alfresco and apply the correct
metadata for indexing.

@author: yasser ansari
"""
from os import walk, path, remove
import re
import jaydebeapi
from jinja2 import Environment, FileSystemLoader
import argparse
import time
from tqdm import tqdm
import logging

logging.basicConfig(filename='dmsrepowalker.log',level=logging.DEBUG,format='%(asctime)s %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p')

# Constants.
META_FILE_POSTFIX = "metadata.properties.xml"

# Set-up for jinja2 templating
CURPATH = path.abspath(path.join(path.dirname(__file__), path.pardir))
env = Environment(
        loader = FileSystemLoader(path.join(CURPATH, 'templates')),
        autoescape=(['xml'])
        )

# Constant for template file name.
MOCK_TEMPLATE_NAME = "template.mock.xml"

# Strings which would be in the file name which determine document type.
PORTFOLIO_REPORT = 'Portfolio Report'
ANNUAL_NOMINEE_REPORT = 'Annual Nominee Report'

# iSeries config.
AS400_DRIVER = 'com.ibm.as400.access.AS400JDBCDriver'
ISERIES_CONFIG = (
        'jdbc:as400://tracey.servers.jhc.co.uk;'
        'ccsid=285;'
        'translate binary=true;'
        'naming=system;'
        'prompt=false;'
        'libraries=F63QULDJBS,QTEMP,F63QUALITY,F63FIGARO,QGPL,JHCJUTIL,SUTIL'
        )
ISERIES_USER = #TODO
ISERIES_PASSWORD = #TODO

JT400_PATH = path.join(CURPATH, 'jt400','jt400.jar')

class reg(object):
    """Class which allows you to access cursors by field name.
    
    The field names, which come from cursor.description, in the row are set as 
    attributes to access the field's value. 
    """
    
    def __init__(self, cursor, row):
        """Constructor for reg class.        
        
        Parameters
        ----------
        cursor: Cursor
            A cursor instance from the jaydebeapi library.
        row: List
            A list of fields.
        """
        for (attr, val) in zip((d[0] for d in cursor.description), row) :
            setattr(self, attr, val)


def find_figaro_account(external_client):
    """Fetch figaro account from CLIXREF using the external client ref.
    
    Parameters
    ----------
    external_client: String
        The external client reference.
    """    
    
    main_client = "C" + external_client.lstrip('0')
        
    cursor = connection.cursor()   
    sql = '''
        select JHCCLI, EXTCLI, EXTFRM 
        from CLIXREF
        where EXTCLI = %s
        ''' % ("'" + main_client + "'")    
        
    cursor.execute(sql)    
    row = cursor.fetchone()
    
    if row is not None:
        r = reg(cursor, row)
        figaro_client = r.JHCCLI
        logging.info("Figaro Client relating to %s is %s" % (external_client, figaro_client))        
    else:
        logging.error("Failed to fetch account " + main_client)
        
        secondary_client = "I" + external_client.lstrip('0')
        
        sql = '''
            select JHCCLI, EXTCLI, EXTFRM 
            from CLIXREF
            where EXTCLI = %s
            ''' % ("'" + secondary_client + "'") 
            
        cursor.execute(sql)    
        row = cursor.fetchone()
        
        if row is not None:
            r = reg(cursor, row)
            figaro_client = r.JHCCLI
            logging.info("Figaro Client relating to %s is %s" % (external_client, figaro_client))     
        else:        
            logging.error("Failed to fetch account " + secondary_client)
            figaro_client = ""
            logging.info("No records in CLIXREF for external account %s" 
                       % external_client)                
    
    cursor.close()
    
    return figaro_client


def find_figaro_person(figaro_account):
    """Fetch person from ROTCLI using the account reference.
    
    Parameters
    ----------
    figaro_account: String
        The account reference.   
    """
    
    cursor = connection.cursor()     
    sql = '''
        select ROTCOD
        from ROTCLI 
        where CLINUM = '%s'
        and RLNKTP = 'OWN'
        and ROTCOD IS NOT NULL
        ''' % (figaro_account)
    try:
        cursor.execute(sql)   
        row = cursor.fetchone()
    except:
        logging.error("Failed to fetch account")
    
    if row is not None:
        r = reg(cursor, row)
        figaro_person = r.ROTCOD
        logging.info("Figaro Person relating to client %s is %s" 
                     % (figaro_account, figaro_person))
    else:
        logging.info("No records in ROTCLI for figaro account %s" % figaro_account) 
        figaro_person = ""
    
    cursor.close()
  
    return figaro_person


def parse_file_name(name):   
    """Parse file name to extract relevant information.
    
    Checks if the file name includes 'Annual Nominee Report' or 'Portfolio 
    Report' to determine the type of document.  
    
    Parameters
    ----------
    name: String
        The filename being parsed.     
    """
    #print("\nLOG - SCANNING DOCUMENT: " + name)    
    if ANNUAL_NOMINEE_REPORT.lower() in name.lower():   
        doc_type = 'bespoke-anr'
        # 000068-E-C-BPL-!!!-Annual Nominee Report 2012-V-20120417-0001.pdf
        regex_pattern = re.compile("([0-9]{6})-(.*)-%s(.*)([0-9]{4})([0-9]{2})([0-9]{2})-([0-9]{2})([0-9]{2})" % ANNUAL_NOMINEE_REPORT) 
               
        try:
            external_client_name = regex_pattern.match(name).group(1)
        except AttributeError:
            raise AttributeError(name)        
        
        external_branch = regex_pattern.match(name).group(2)
        doc_name = '%s%s' % (ANNUAL_NOMINEE_REPORT, regex_pattern.match(name).group(3)) 
        year = regex_pattern.match(name).group(4)
        if int(year) > int(time.strftime("%Y")):      
            raise ValueError('year should not be in the future')
        month = regex_pattern.match(name).group(5)
        if not 1 <= int(month) <= 12:
            raise ValueError('month is incorrect')
        day = regex_pattern.match(name).group(6)      
        if not 1 <= int(day) <= 31:
            raise ValueError('day is incorrect')            
        hour = regex_pattern.match(name).group(7)
        if not 0 <= int(hour) <= 23:
            hour = '00';
        minutes = regex_pattern.match(name).group(8)             
        if not 0 <= int(minutes) <= 59:
            minutes = '00';
        doc_datetime = '%s-%s-%sT%s:%s' % (year, month, day, hour, minutes)
        figaro_account = find_figaro_account(external_client_name)        
        figaro_person = find_figaro_person(figaro_account)                    

    elif PORTFOLIO_REPORT.lower() in name.lower():                 
        doc_type = 'valuation'
        # 141739-CTG-!!!-Portfolio Report July 2016-P-20160819-0001-072016.pdf
        regex_pattern = re.compile("([0-9]{6})-(.*)-%s(.*)([0-9]{4})([0-9]{2})([0-9]{2})-([0-9]{2})([0-9]{2})" % PORTFOLIO_REPORT)        
        try:
            external_client_name = regex_pattern.match(name).group(1)            
        except AttributeError:            
            raise AttributeError(name)        
        
        external_branch = regex_pattern.match(name).group(2)
        doc_name = '%s%s' % (PORTFOLIO_REPORT, regex_pattern.match(name).group(3))           
        year = regex_pattern.match(name).group(4)        
        if int(year) > int(time.strftime("%Y")):
            raise ValueError('year should not be in the future')            
        month = regex_pattern.match(name).group(5)
        if not 1 <= int(month) <= 12:
            raise ValueError('month is incorrect')
        day = regex_pattern.match(name).group(6) 
        if not 1 <= int(day) <= 31:            
            raise ValueError('day is incorrect')               
        hour = regex_pattern.match(name).group(7)
        if not 0 <= int(hour) <= 23:
            hour = '00';
        minutes = regex_pattern.match(name).group(8)             
        if not 0 <= int(minutes) <= 59:
            minutes = '00';              
        doc_datetime = '%s-%s-%sT%s:%s' % (year, month, day, hour, minutes)    
        figaro_account = find_figaro_account(external_client_name)        
        figaro_person = find_figaro_person(figaro_account)   
    
    return (doc_name, doc_datetime, figaro_account, figaro_person, 
            doc_type) 


def create_metadata_file(root, name, counts):
    """Creates metadata file based on an existing file name.
    
    Parameters
    ----------
    root: String
        The path to where the file is located.
    name: String
        The file name.
    counts: Dictionary
        Contains key 'good' and 'bad' with values for number of good or bad 
        runs.
    """    
    (doc_name, doc_datetime, 
     figaro_account, figaro_person, doc_type) = parse_file_name(name)            
    repofilemeta = path.join(root, '%s.%s' % (name, META_FILE_POSTFIX))
                        
    # Does metadata file already exist? If yes, delete
    if path.isfile(repofilemeta):
        remove(repofilemeta)
        logging.info("Deleted existing metadata file %s" % repofilemeta)

    template = env.get_template('template.mock.xml')

    # create new metadata file            
    with open(repofilemeta, 'w') as f:
        metadef = template.render(
                title = doc_name, 
                description = doc_name,
                person = figaro_person, 
                account = figaro_account,
                figaro_doctype = doc_type,
                datetime = doc_datetime,
                doctype= 'figaro:%s' % doc_type)
        f.write(metadef)
        f.close
        counts['good'] = counts['good'] + 1
        logging.info("Created metadata file %s" % repofilemeta)
        
def delete_shadow_metadata(dirpath, repofile, counts):
    ''' Delete Alfresco shadow metadata file '''
    repofilemeta = path.join(dirpath, repofile)
    try:
        remove(repofilemeta)
    except IOError:
        logging.error("Error deleting file: " + repofile)
        counts['bad'] = counts['bad'] + 1        
    else:
        counts['good'] = counts['good'] + 1

    return counts


def walkdir(root_directory_name, mode):
    """Traverse a directory tree and create metadata for files."""
    
    counts = {'good': 0, 'bad': 0}    
    
    files_to_upload = []
    
    logfilename = 'errored_files %s.txt' % time.strftime("%Y%m%d-%H%M%S")
    
    logfile = open(logfilename, 'w')    
       
    # to improve efficiency                         
    def build_metadata():            
        infolog = logging.info                
        errorlog = logging.error
        writelog = logfile.write
        for dirpath, filename in tqdm(files_to_upload):
            try: 
                create_metadata_file(dirpath, filename, counts)
                infolog("File %s has been parsed" % filename)            
            except Exception:
                errorlog("Invalid file name pattern: %s" % filename)    
                writelog("%s\n" % filename)                
                counts['bad'] = counts['bad'] + 1
                continue
            
    def teardown_metadata():
        infolog = logging.info  
        for dirpath, filename in tqdm(files_to_upload):
            delete_shadow_metadata(dirpath, filename, counts)
            infolog("File %s has been deleted" % filename)            
            
    if mode == "build":
        # filter out metadata files
        for dirpath, dirs, files in walk(root_directory_name):
            for filename in files:
                if META_FILE_POSTFIX not in filename:            
                    files_to_upload.append([dirpath, filename])         
        build_metadata()
    elif mode == "teardown":
        # filter to include only metadata files
        for dirpath, dirs, files in walk(root_directory_name):
            for filename in files:
                if META_FILE_POSTFIX in filename:            
                    files_to_upload.append([dirpath, filename])    
        teardown_metadata()
    else:
        tqdm.write("Error: unknown mode")
        logging.error("Error: Unkown mode")
    
    
        
    # Print a summary of the run
    statustext = "SUMMARY RECAP "
    statuslen = len(statustext)
    statuspaddinglen = int(80 - statuslen)
    statusline = statustext + "*" * statuspaddinglen
    goodcount = str(counts['good'])
    goodlen = len(goodcount)
    goodpaddinglen = int(29 - goodlen)
    badcount = str(counts['bad'])
    badlen = len(badcount)
    badpaddinglen = int(29 - badlen)
    m, s = divmod(time.time() - start_time, 60)
    h, m = divmod(m, 60)    
    tqdm.write("\n\n\n")
    tqdm.write(statusline)
    tqdm.write("Total number of successful file name scans -------" + "-" * 
          goodpaddinglen + " " + goodcount)
    tqdm.write("Total number of failed file name scans -----------" + "-" * 
          badpaddinglen + " " + badcount)
    tqdm.write("Program took %d hours %02d minutes and %02.2f seconds" % (h, m, s))
    tqdm.write("*" * 80)
        
    logfile.close()

def main(args):
    walkdir(args.root, args.mode)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--root", help="""Root directory of the source 
                        file system structure""", default=".", required=True)
    parser.add_argument("-m", "--mode", help="""Mode of execution; build or 
                        teardown""", default="build", 
                        required=True)
    args = parser.parse_args()
    start_time = time.time()
    try:
        connection = jaydebeapi.connect(
                AS400_DRIVER, 
                ISERIES_CONFIG, 
                [ISERIES_USER, ISERIES_PASSWORD], 
                JT400_PATH)        
    except:
        logging.info("An error occured connecting to the iSeries")
    
    main(args)
    
    connection.close()  
    
