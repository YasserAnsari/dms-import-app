# -*- coding: utf-8 -*-
"""
Test templater

@author: AnsariY
"""
from jinja2 import Environment, FileSystemLoader, select_autoescape
import os

TITLE = 'Some title'
DESCRIPTION = 'Annual Report'
PERSON = '000000000004'
ACCOUNT = '0000683'

CURPATH = os.path.dirname(os.path.abspath(__file__))

env = Environment(
        loader = FileSystemLoader(os.path.join(CURPATH, 'templates')),
        autoescape=(['xml'])
        )

template = env.get_template('template.mock.xml')

print(template.render(title=TITLE, description=DESCRIPTION, person=PERSON, account=ACCOUNT))

repofilemeta = os.path.join(CURPATH, 'templates', '')

        # Does metadata file already exist? If yes, delete
        if os.path.isfile(repofilemeta):
            os.remove(repofilemeta)