# -*- coding: utf-8 -*-
"""DMS Importer tool.

Created on Wed Nov  8 11:02:02 2017

This tool can be used to examine a file system folder structure and the
documents contained within it. It's primary purpose is to create a sidecar
XML file for each document that contains Figaro metadata information about
that document.

The metadata associated with a document is inferred from the file naming
convention, as provided by a client.

Once metadata sidecar files have been generated the Alfresco bulk upload
tool can be used to ingest all documents into Alfresco and apply the correct
metadata for indexing.

@author: yasser ansari
"""
from os import walk, path, remove
import re
import jaydebeapi
from jinja2 import Environment, FileSystemLoader
import argparse
import time

print("The program is beginning")

# Constants.
META_FILE_POSTFIX = "metadata.properties.xml"

# Set-up for jinja2 templating
CURPATH = path.abspath(path.join(path.dirname(__file__), path.pardir))
env = Environment(
        loader = FileSystemLoader(path.join(CURPATH, 'templates')),
        autoescape=(['xml'])
        )

# Constant for template file name.
MOCK_TEMPLATE_NAME = "template.mock.xml"

# Strings which would be in the file name which determine document type.
PORTFOLIO_REPORT = 'Portfolio Report'
ANNUAL_NOMINEE_REPORT = 'Annual Nominee Report'

# iSeries config.
AS400_DRIVER = 'com.ibm.as400.access.AS400JDBCDriver'
ISERIES_CONFIG = (
        'jdbc:as400://tracey.servers.jhc.co.uk;'
        'ccsid=285;'
        'translate binary=true;'
        'naming=system;'
        'prompt=false;'
        'libraries=F63QULDJBS,QTEMP,F63QUALITY,F63FIGARO,QGPL,JHCJUTIL,SUTIL'
        )
ISERIES_USER = 'ANSARIY'
ISERIES_PASSWORD = 'IRASNAR' 

JT400_PATH = path.join(CURPATH, 'jt400','jt400.jar')
print(JT400_PATH)


class reg(object):
    """Class which allows you to access cursors by field name.
    
    The field names, which come from cursor.description, in the row are set as 
    attributes to access the field's value. 
    """
    
    def __init__(self, cursor, row):
        """Constructor for reg class.        
        
        Parameters
        ----------
        cursor: Cursor
            A cursor instance from the jaydebeapi library.
        row: List
            A list of fields.
        """
        for (attr, val) in zip((d[0] for d in cursor.description), row) :
            setattr(self, attr, val)


def find_figaro_account(external_client):
    """Fetch figaro account from CLIXREF using the external client ref.
    
    Parameters
    ----------
    external_client: String
        The external client reference.
    """    
    try:
        connection = jaydebeapi.connect(
                AS400_DRIVER, 
                ISERIES_CONFIG, 
                [ISERIES_USER, ISERIES_PASSWORD], 
                JT400_PATH)        
    except:
        print("An error occured connecting to the iSeries")
    cursor = connection.cursor()   
    sql = '''
        select JHCCLI, EXTCLI, EXTFRM 
        from CLIXREF
        where EXTCLI = %s
        ''' % (external_client)    

    cursor.execute(sql)    
    row = cursor.fetchone()
    
    if row is not None:
        r = reg(cursor, row)
        figaro_client = r.JHCCLI
        print("Figaro Client relating to " + external_client + " is " 
              + figaro_client)
        cursor.close()
        connection.close()   
        return figaro_client
    else:
        print("No records in CLIXREF for external account " + external_client)        
        cursor.close()
        connection.close()   
        return ""


def find_figaro_person(figaro_account):
    """Fetch person from ROTCLI using the account reference.
    
    Parameters
    ----------
    figaro_account: String
        The account reference.   
    """
    connection = jaydebeapi.connect(
            AS400_DRIVER, 
            ISERIES_CONFIG, 
            [ISERIES_USER, ISERIES_PASSWORD], 
            JT400_PATH)   
    cursor = connection.cursor()     
    sql = '''
        select ROTCOD
        from ROTCLI 
        where CLINUM = '%s'
        and ROTCOD IS NOT NULL
        ''' % (figaro_account)
    
    cursor.execute(sql)   
    row = cursor.fetchone()
    
    if row is not None:
        r = reg(cursor, row)
        figaro_person = r.ROTCOD
        print("Figaro Person relating to client " + figaro_account + " is " 
              + figaro_person)    
        cursor.close()
        connection.close()    
        return figaro_person
    else:
        cursor.close()
        connection.close()    
        return ""


def parse_file_name(name):   
    """Parse file name to extract relevant information.
    
    Checks if the file name includes 'Annual Nominee Report' or 'Portfolio 
    Report' to determine the type of document.  
    
    Parameters
    ----------
    name: String
        The filename being parsed.     
    """
    print("\nLOG - SCANNING DOCUMENT: " + name)    
    if ANNUAL_NOMINEE_REPORT.lower() in name.lower():   
        doc_type = 'bespoke-anr'
        # 000068-E-C-BPL-!!!-Annual Nominee Report 2012-V-20120417-0001.pdf
        regex_pattern = re.compile("([0-9]{6})-(.*)-" + 
                                   ANNUAL_NOMINEE_REPORT + 
                                   ("(.*)([0-9]{4})([0-9]{2})([0-9]{2})-"
                                    "([0-9]{2})([0-9]{2})"))            
        
        try:
            external_client_name = regex_pattern.match(name).group(1)
        except AttributeError:
            raise AttributeError(name)        
        
        external_branch = regex_pattern.match(name).group(2)
        doc_name = (ANNUAL_NOMINEE_REPORT + 
                        regex_pattern.match(name).group(3))        
        year = regex_pattern.match(name).group(4)
        month = regex_pattern.match(name).group(5)
        day = regex_pattern.match(name).group(6)        
        hour = regex_pattern.match(name).group(7)
        minutes = regex_pattern.match(name).group(8)             
        doc_datetime = (year + '-' + month + '-' + day + 'T' + hour + ':' 
                        + minutes)
        figaro_account = find_figaro_account(external_client_name)        
        figaro_person = find_figaro_person(figaro_account)   
        
        return (doc_name, doc_datetime, figaro_account, figaro_person, 
                doc_type)    
    elif PORTFOLIO_REPORT.lower() in name.lower():         
        doc_type = 'valuation'
        # 141739-CTG-!!!-Portfolio Report July 2016-P-20160819-0001-072016.pdf
        regex_pattern = re.compile("([0-9]{6})-(.*)-" 
                                   + PORTFOLIO_REPORT
                                   + ("(.*)([0-9]{4})([0-9]{2})([0-9]{2})-"
                                      "([0-9]{2})([0-9]{2})"))        
        try:
            external_client_name = regex_pattern.match(name).group(1)            
        except AttributeError:
            raise AttributeError(name)        
        
        external_branch = regex_pattern.match(name).group(2)
        doc_name = PORTFOLIO_REPORT + regex_pattern.match(name).group(3)           
        year = regex_pattern.match(name).group(4)
        month = regex_pattern.match(name).group(5)
        day = regex_pattern.match(name).group(6)                
        hour = regex_pattern.match(name).group(7)
        minutes = regex_pattern.match(name).group(8)                
        doc_datetime = (year + '-' + month + '-' + day + 'T' + hour + ':' 
                        + minutes)        
        figaro_account = find_figaro_account(external_client_name)        
        figaro_person = find_figaro_person(figaro_account)   
        return (doc_name, doc_datetime, figaro_account, figaro_person, 
                doc_type)    
    else:
        return None


def create_metadata_files(root, name, counts):
    """Creates metadata file based on an existing file name.
    
    Parameters
    ----------
    root: String
        The path to where the file is located.
    name: String
        The file name.
    counts: Dictionary
        Contains key 'good' and 'bad' with values for number of good or bad 
        runs.
    """    
    (doc_name, doc_datetime, 
     figaro_account, figaro_person, doc_type) = parse_file_name(name)            
    repofilemeta = path.join(root, name + '.' + META_FILE_POSTFIX)
                        
    # Does metadata file already exist? If yes, delete
    if path.isfile(repofilemeta):
        remove(repofilemeta)
        print("Deleted existing metadata file " + repofilemeta)

    template = env.get_template('template.mock.xml')

    # create new metadata file            
    with open(repofilemeta, 'w') as f:
        metadef = template.render(
                title = doc_name, 
                description = doc_name,
                person = figaro_person, 
                account = figaro_account,
                figaro_doctype = doc_type,
                datetime = doc_datetime,
                doctype= 'figaro:' + doc_type)
        f.write(metadef)
        f.close
        counts['good'] = counts['good'] + 1
        print("Created metadata file " + repofilemeta)


def walkdir(root_directory_name):
    """Traverse a directory tree and create metadata for files."""
    
    counts = {'good': 0, 'bad': 0}
    
    errored_files = []
    
    for root, dirs, files in walk(root_directory_name, topdown=True):        
        for name in files:            
            if META_FILE_POSTFIX not in name:
                try:
                    create_metadata_files(root, name, counts)
                    print("File " + name + " has been parsed")            
                except Exception:
                    print("Invalid file name pattern: " + name)                    
                    errored_files.append(path.join(root_directory_name, name))
                    counts['bad'] = counts['bad'] + 1
                    continue
        
    # Print a summary of the run
    statustext = "SUMMARY RECAP "
    statuslen = len(statustext)
    statuspaddinglen = int(80 - statuslen)
    statusline = statustext + "*" * statuspaddinglen
    goodcount = str(counts['good'])
    goodlen = len(goodcount)
    goodpaddinglen = int(29 - goodlen)
    badcount = str(counts['bad'])
    badlen = len(badcount)
    badpaddinglen = int(29 - badlen)
    print("\n\n\n")
    print(statusline)
    print("Total number of successful file name scans -------" + "-" * 
          goodpaddinglen + " " + goodcount)
    print("Total number of failed file name scans -----------" + "-" * 
          badpaddinglen + " " + badcount)
    print("*" * 80)
    
    logfile = open('errored_files.txt', 'w')

    for item in errored_files:
        logfile.write("%s\n" % item)
        
    logfile.close()

def main(args):
    walkdir(args.root)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-r", "--root", help="""Root directory of the source 
                        file system structure""", default=".", required=True)
    args = parser.parse_args()
    start_time = time.time()
    main(args)
    print("--- Program took %s seconds ---" % (time.time() - start_time))
